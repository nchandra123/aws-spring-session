package com.certgurus.redis.knackerred.controller;

import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import redis.clients.jedis.Jedis;

@RestController
public class HomeController {
	
	private static final String REDIS_HOST_NAME = "localhost";
	private static final int REDIS_PORT = 6380;
	
	@Autowired
	JedisConnectionFactory jedisConnectionFactory;
	
	@RequestMapping("/add")
	public String helloAdmin(HttpSession session) {
		
		//final HttpSession session = request.getSession(); //<---- Remember, this doesnt work
		session.setAttribute("red_key1", "red_val11");
		
		System.out.println(" -------------> session ID: " + session.getId());
		System.out.println(" -------------> session getAttributeNames: " + session.getAttributeNames());
		
		System.out.println("Wrote to session ");
		
		return "hello admin";
	}
	
	@RequestMapping("/redis")
	public String helloSession(HttpSession session) {
		final Object attrValue = session.getAttribute("red_key1");
		
		System.out.println("-------------> Value from session for red_key1: " + attrValue);
		
		return "redis session";
	}
	
	@RequestMapping("/keys")
	public String getKeys(HttpSession session) {
		final RedisConnection connection = jedisConnectionFactory.getConnection();
		
		System.out.println("Redis conn, not jedis-conn --------------------> " + connection);
		
		// This is Jedis client
		Jedis jedis = new Jedis(REDIS_HOST_NAME, REDIS_PORT); //<--- Start Redis server for this Jedis client to work. Otherwise where will this client connect?
		Set<String> result = jedis.keys("*");
		System.out.println("\n------------------- Redis keys: " + result);
		
		/// ------- Print redis hkeys
		jedis.keys("*").forEach(thisKey -> {
			System.out.println("---->>>> key: " + thisKey);
			
			if(thisKey.contains("spring:session:sessions:expires")){
				System.out.println("-- key expiry entry ---");
			}
			else if(thisKey.contains("spring:session:sessions")){
				System.out.println("---->> hkey: " + jedis.hkeys(thisKey));
				jedis.hkeys(thisKey).forEach(thisEntry -> {
					System.out.println("---> Value String: " + thisEntry);
				});
			}
			
		});
		///
		
		return "redis keys";
	}
	
	
	@RequestMapping("/logout")
	public String logOutNow(HttpSession session) {
		
		session.invalidate();
		
		Jedis jedis = new Jedis("localhost", 6380); //<--- Start Redis server for this Jedis client to work. Otherwise where will this client connect?
		
		Set<String> result = jedis.keys("*");
		
		System.out.println("\n After logout ------------------- Redis keys: " + result);
		
		return "redis logged out";
	}
}
